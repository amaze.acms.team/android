package com.example.musicstream;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface ApiInterface {
    @GET("/rec")
    Call<List<uploadd>> getUser();
    @GET("/getrecording/{id}")
    Call<uploadd> getTodo(@Path("id") int id);
    @POST("/postrecording")
    Call<uploadd> postreq(@Body uploadd up);
}

