package com.example.musicstream;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {
    Button next_button,loginfb;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        next_button = (Button)findViewById(R.id.but);
        next_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final EditText txt_name = (EditText)findViewById(R.id.name);
                final String name = txt_name.getText().toString();

                final EditText txt_id =(EditText)findViewById(R.id.ID);
                final String id = txt_id.getText().toString();

                final EditText txt_song =(EditText)findViewById(R.id.sngname);
                final String song_title = txt_song.getText().toString();

                final EditText txt_date =(EditText)findViewById(R.id.recdt);
                final String rec_date = txt_date.getText().toString();
                int f=0;
                if(name.length()==0)
                {
                    txt_name.requestFocus();
                    txt_name.setError("FIELD CANNOT BE EMPTY");
                }
                else if(!name.matches("[a-zA-Z ]+"))
                {
                    txt_name.setError("ENTER ONLY ALPHABETICAL CHARACTER");
                }
                else
                    f++;
                if(id.length()==0)
                {
                    txt_name.requestFocus();
                    txt_name.setError("FIELD CANNOT BE EMPTY");
                 }
                else
                    f++;
                if(song_title.length()==0)
                {
                    txt_song.requestFocus();
                    txt_song.setError("FIELD CANNOT BE EMPTY");
                }
                else
                    f++;
                if(rec_date.length()==0)
                {
                    txt_date.requestFocus();
                    txt_date.setError("FIELD CANNOT BE EMPTY");
                }
                else
                    f++;
                if(f==4) {
                    Intent intent = new Intent(MainActivity.this, Musicc2.class);
                    intent.putExtra("data1",id);
                    intent.putExtra("data2",song_title);
                    startActivity(intent);
                }
            }
        });
        loginfb = (Button)findViewById(R.id.logfb);
        loginfb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent inte = new Intent(MainActivity.this,facebookk.class);
                startActivity(inte);
            }
        });
    }
}
