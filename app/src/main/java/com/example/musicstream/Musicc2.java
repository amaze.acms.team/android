package com.example.musicstream;

import android.Manifest;
import android.Manifest;
import android.content.ContentProvider;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

import android.content.Context;
import android.widget.Toast;
import android.widget.Toolbar;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import org.w3c.dom.Text;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
public class Musicc2 extends AppCompatActivity {
    private static final int AUDIO_REQUEST = 10;
    static int request_code = 1001;
    TextView txt_path,txt_id,txt_song;
    Button choose_file_btn,play_button,pause_button,stop_button,upload_btn;
    Intent intent1;
    private final String TAG ="Mainactivty";
    public String path="";
    ByteArrayOutputStream bos = new ByteArrayOutputStream();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_musicc2);

        //-----------displaying id and song name from previous activity------------
        final int userid;
        txt_id=(TextView)findViewById(R.id.sid);
        String id="User id : ";
        String songname="Song name : ";
        txt_song=(TextView)findViewById(R.id.ssn);
        String id1= getIntent().getStringExtra("data1");
        id+=id1;
        if(Character.isDigit(id1.charAt(0))){
            userid = Integer.parseInt(id1);
        }
        else
            userid = 0;
        final String songNam = getIntent().getStringExtra("data2");
        songname+=songNam;
        txt_id.setText(id);
        txt_song.setText(songname);

        //------permission to access audio files---------
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, request_code);

        txt_path = (TextView) findViewById(R.id.txt) ;
        choose_file_btn = (Button) findViewById(R.id.btn);
        choose_file_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent1 = new Intent(Intent.ACTION_GET_CONTENT);
                intent1.setType("*/*");
                startActivityForResult(intent1, 10);
            }
        });
        upload_btn = (Button)findViewById(R.id.button2);
        upload_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getAllusers();//----get all the user details----
                getSpecificId(userid);//----get the specific userid details----
                postdet(userid,songNam,bos);//---post data-----
            }
        });

    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case 10:
                if (resultCode == RESULT_OK) {
                    Uri uri1 = data.getData();
                    path = data.getData().getPath();
                    //getting the path of the file
                    String filePath = null;
                    Uri _uri = data.getData();
                    Log.d("", "URI = " + _uri);
                    if (_uri != null && "content".equals(_uri.getScheme())) {
                        Cursor cursor = this.getContentResolver().query(_uri, new String[]{MediaStore.Audio.AudioColumns.DATA}, null, null, null);
                        cursor.moveToFirst();
                        filePath = cursor.getString(0);
                        cursor.close();
                    }
                    else {
                        filePath = _uri.getPath();
                    }
                    //--------------converting audio file into stream------------
                    FileInputStream fs = null;
                    try {
                        fs = new FileInputStream(filePath);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                    byte[] bb = new byte[1024];
                    try {
                        for (int readNum; (readNum = fs.read(bb)) != -1; ) {
                            bos.write(bb, 0, readNum);
                        }
                    } catch (IOException ae) {
                        ae.printStackTrace();
                    }
                    byte[] bytes = bos.toByteArray();
                    playMusic(filePath);
                    //readfile();
                }
        }
    }
    //-----------------Media Player--------------------
    public void playMusic(String filePath)
    {
        final MediaPlayer mp = new MediaPlayer();
        try {
            mp.setDataSource(filePath);
            mp.prepare();
        } catch (IOException e) {
            e.printStackTrace();
        }
        txt_path.setText(filePath);
        play_button = (Button)findViewById(R.id.button3);
        pause_button = (Button)findViewById(R.id.button4);
        stop_button = (Button)findViewById(R.id.button5);

        play_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mp.start();
            }
        });
        pause_button.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                mp.pause();
            }
        });
        stop_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mp.stop();
            }
        });
    }
    //----------------get the specific userid  details and audio files---------------
    public void getSpecificId(int id)
    {
        Call<uploadd> c = retro.getUserService().getTodo(id);
        c.enqueue(new Callback<uploadd>() {
            @Override
            public void onResponse(Call<uploadd> call, Response<uploadd> response) {
                Log.e(TAG,"Onresponse"+response.body());
                String val = response.body().toString();
                Toast.makeText(Musicc2.this,"Done "+response.body(),Toast.LENGTH_LONG).show();
                TextView tt = (TextView)findViewById(R.id.tv);
                tt.setText(val);
                String song_url = response.body().getUrl();
                playMusic(song_url);
            }
            @Override
            public void onFailure(Call<uploadd> call, Throwable t) {
                Toast.makeText(Musicc2.this,"fail ",Toast.LENGTH_LONG).show();

            }
        });
    }
    //----------------display all the user details---------------
    public void getAllusers(){
        Call<List<uploadd>> call = retro.getUserService().getUser();
        call.enqueue(new Callback<List<uploadd>>() {
            @Override
            public void onResponse(Call<List<uploadd>> call, Response<List<uploadd>> response) {
                Toast.makeText(Musicc2.this,"Done "+response.body(),Toast.LENGTH_LONG).show();
                Log.e(TAG,"Onresponse"+response.body());
            }

            @Override
            public void onFailure(Call<List<uploadd>> call, Throwable t) {
                Log.e(TAG,"Onfailure"+t.getLocalizedMessage());
                Toast.makeText(Musicc2.this,"fail ",Toast.LENGTH_LONG).show();
            }
        });
    }
    //------------------------post the data to server--------------------
    public void postdet(int id,String recordName,ByteArrayOutputStream bb) {
        uploadd uplo = new uploadd(id,recordName,bb);
        Call<uploadd> uu = retro.getUserService().postreq(uplo);
        uu.enqueue(new Callback<uploadd>() {
            @Override
            public void onResponse(Call<uploadd> call, Response<uploadd> response) {
                Log.e(TAG, "Onresponse" + response.body());
                Toast.makeText(Musicc2.this, "Done " + response.body(), Toast.LENGTH_LONG).show();
                String va = response.body().toString();
                TextView tt = (TextView) findViewById(R.id.tv);
                tt.setText(va);
            }

            @Override
            public void onFailure(Call<uploadd> call, Throwable t) {
                Toast.makeText(Musicc2.this, "fail ", Toast.LENGTH_LONG).show();

            }
        });
    }
    //-----------------------function to read the text file-------------
    public void readfile () {
        StringBuilder bb = new StringBuilder();
        try {
            BufferedReader br = new BufferedReader(new FileReader(path));
            String str;
            while ((str = br.readLine()) != null) {
                bb.append(str);
                bb.append("\n");
            }
            br.close();
        } catch (Exception ae) {
            Log.e(TAG, "error is" + ae.toString());
        }
        Log.e(TAG, "read text is\n" + bb.toString());
        txt_path.setText(bb.toString());
    }
}