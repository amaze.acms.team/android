package com.example.musicstream;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

import de.hdodenhof.circleimageview.CircleImageView;
public class facebookk extends AppCompatActivity {
    LoginButton loginButton;
    Button nxt_btn;
    CircleImageView circleImageView;
    TextView txtname,txtemail;
    CallbackManager callbackManager;
    String email="";
    EditText songnam;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_facebookk);

                loginButton = findViewById(R.id.logbut);
                txtname = findViewById(R.id.name);
                txtemail = findViewById(R.id.email);
                circleImageView = findViewById(R.id.prof);
                callbackManager = CallbackManager.Factory.create();
                loginButton.setPermissions(Arrays.asList("email","public_profile"));
                loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {

                    }

                    @Override
                    public void onCancel() {

                    }

                    @Override
                    public void onError(FacebookException error) {

                    }
                });
                nxt_btn = (Button)findViewById(R.id.nxtbutton);
                nxt_btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent int1 = new Intent(facebookk.this,Musicc2.class);
                        int1.putExtra("data1",email);
                        songnam = (EditText)findViewById(R.id.songname);
                        final String song_title =  songnam.getText().toString();
                        int1.putExtra("data2",song_title);
                        startActivity(int1);
                    }
                });
    }

            @Override
            protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
                callbackManager.onActivityResult(requestCode,resultCode,data);
                super.onActivityResult(requestCode, resultCode, data);
            }
            AccessTokenTracker tokenTracker = new AccessTokenTracker() {
                @Override
                protected void onCurrentAccessTokenChanged(AccessToken oldAccessToken, AccessToken currentAccessToken) {
                    if(currentAccessToken==null)
                    {
                        txtname.setText("");
                        txtemail.setText("");
                        circleImageView.setImageResource(0);
                        Toast.makeText(facebookk.this,"User Logged Out",Toast.LENGTH_LONG).show();
                    }
                    else{
                        profile(currentAccessToken);
                    }
                }
            };
            public void profile(AccessToken newAcess){
                GraphRequest request = GraphRequest.newMeRequest(newAcess, new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        try {
                            String fn = object.getString("first_name");
                            String ln = object.getString("last_name");
                            email = object.getString("email");
                            String id = object.getString("id");
                            String im_url = "https://graph.facebook.com/"+id+ "/picture?type=normal";
                            txtemail.setText(email);
                            txtname.setText(fn +" " +ln);
                            RequestOptions requestOptions = new RequestOptions();
                            requestOptions.dontAnimate();
                            Glide.with(facebookk.this).load(im_url).into(circleImageView);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                });
                Bundle pp = new Bundle();
                pp.putString("fields","first_name,last_name,email,id");
                request.setParameters(pp);
                request.executeAsync();


            }
            public void clb(){
                if(AccessToken.getCurrentAccessToken()!=null){
                    profile(AccessToken.getCurrentAccessToken());
                }
            }
}
