package com.example.musicstream;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class retro {
    public static Retrofit getRetrofit() {
        String url ="http://52.4.200.81:8080/alog/webapi/rec";
        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient okHttpClient = new OkHttpClient.Builder().addInterceptor(httpLoggingInterceptor).build();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create()).build();
        return retrofit;
    }
    public static ApiInterface getUserService() {
        ApiInterface userservice = getRetrofit().create(ApiInterface.class);
        return userservice;
    }
}
