package com.example.musicstream;

import java.io.ByteArrayOutputStream;

public class uploadd {
    private int id;
    private int userId;
    private String recordName;
    private ByteArrayOutputStream bb;
    private String url;
    public uploadd(int id, int userId, String recordName, String url) {
        this.id = id;
        this.userId = userId;
        this.recordName = recordName;
        this.url = url;
    }
    public uploadd(int userId, String recordName, ByteArrayOutputStream bb) {
        this.userId = userId;
        this.recordName = recordName;
        this.bb = bb;
    }
    public String getUrl(){
        return url;
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getTitle() {
        return recordName;
    }

    public void setTitle(String recordName) {
        this.recordName = recordName;
    }

    public ByteArrayOutputStream getCompleted() {
        return bb;
    }

    public void setCompleted(ByteArrayOutputStream bb) {
        this.bb = bb;
    }

    @Override
    public String toString() {
        return "uploadd{" +
                "id=" + id +
                ", userId=" + userId +
                ", title='" + recordName + '\'' +
                '}';
    }
}